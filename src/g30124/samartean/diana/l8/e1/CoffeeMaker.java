package g30124.samartean.diana.l8.e1;

class CoffeeMaker {
    static int numberOfCoffees = 0;
    
    Coffee makeCoffee () throws NumberException {
        if(numberOfCoffees > 10 )
            throw new NumberException(numberOfCoffees,"Enough coffees");

        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        numberOfCoffees ++ ;
        return coffee;
    }

}
