package g30124.samartean.diana.l8.e4;

import java.io.*;

public class Serializare {
    public static void main(String[] args) throws Exception {
    AlienFactory f=new AlienFactory();
    Alien a=f.createALien("axx");
    Alien b=f.createALien("abb");
    f.freezAlien(a,"aliena.dat");
    f.freezAlien(b,"alienb.dat");
    Alien x=f.unfreezeAlien("alienb.dat");
    Alien y=f.unfreezeAlien("aliena.dat");
        System.out.println(x);
        System.out.println(y);

    }
    static class AlienFactory{
        Alien createALien(String name){
            Alien z=new Alien(name);
            System.out.println(z+" traieste" );
            return z;
        }
        void freezAlien (Alien a,String storeRecipientName) throws IOException{
            ObjectOutputStream o=new ObjectOutputStream(new FileOutputStream(storeRecipientName));
            o.writeObject(a);
            System.out.println(a+" brb");
        }
        Alien unfreezeAlien( String storeRecipientName) throws IOException, ClassNotFoundException {
            ObjectInputStream in=new ObjectInputStream(new FileInputStream(storeRecipientName));
            Alien x=(Alien)in.readObject();
            System.out.println(x+" M-am intors");
            return x;
        }
    }
    static class Alien implements Serializable{
        String name;
        transient int id;
        public Alien (String n)
        {
            this.name=n;
            id=(int) (Math.random()*100);

        }
        public void move()
        {
            System.out.println("Aline is moving"+this);
        }

        @Override
        public String toString() {
            return "Alien{" +
                    "name='" + name + '\'' +
                    ", id=" + id +
                    '}';
        }
    }
}
