package g30124.samartean.diana.l8.e4;

public class NumberException extends Exception {
    int nr;
    public NumberException(String msj,int nr) 
    {
        super(msj);
        this.nr=nr;

    }

    public int getNr() {
        return nr;
    }
}
