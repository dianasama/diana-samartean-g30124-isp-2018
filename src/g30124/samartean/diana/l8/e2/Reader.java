package g30124.samartean.diana.l8.e2;

import java.io.*;
import java.util.Scanner;

public class Reader {
    public static void main(String[] args) throws IOException {
        System.out.println("Introduceti litera cautata: ");
        char c=(char) System.in.read();
        File file=new File("date.txt");
        BufferedReader bf=new BufferedReader(new FileReader(file));
        String line;
        int aparitii=0;
        while((line=bf.readLine())!=null)
        {
            int line_counter=line.length();
            for(int i=0;i<line_counter;i++)
                if(line.charAt(i)==c)
                    aparitii++;
        }
        System.out.println("Avem " + aparitii +" aparitii de " + c);

    }
}
