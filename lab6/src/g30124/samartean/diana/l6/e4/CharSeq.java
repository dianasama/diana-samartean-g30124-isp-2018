package g30124.samartean.diana.l6.e4;

import static java.lang.System.in;

public class CharSeq implements java.lang.CharSequence {
    private char[] ch;

    public CharSeq(char[] ch) {
        this.ch = ch;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public char charAt(int index) {
        return ch[index];
    }

    @Override
    public java.lang.CharSequence subSequence(int start, int end) {
        return null;
    }
}
