package g30124.samartean.diana.l6.e5;

import javax.swing.*;
import java.awt.*;

public class DrawingBoard  extends JFrame {

    Pyramid[] pyramid = new Pyramid[50];

    public DrawingBoard() {
        super();
        this.setTitle("Drawing board example");
        this.setSize(500,500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);

    }

    public void addPyramid(Pyramid p1){
        for(int i=0;i<pyramid.length;i++){
            if(pyramid[i]==null){
                pyramid[i] = p1;
                break;
            }
        }
        this.repaint();
    }

    public void paint(Graphics g){
        for(int i=0;i<pyramid.length;i++){
            if(pyramid[i]!=null)
                pyramid[i].draw(g);
        }
    }
}
