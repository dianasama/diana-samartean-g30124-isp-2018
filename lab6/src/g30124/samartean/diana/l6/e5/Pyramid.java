package g30124.samartean.diana.l6.e5;

import java.awt.*;
import java.util.Scanner;

import java.awt.*;

public class Pyramid {
    
	private int height, width, x, y;

    public Pyramid(int height, int width,int x,int y) {
        this.height = height;
        this.width = width;
        this.x = x;
        this.y = y;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public void draw(Graphics g) {
    	g.drawRect(x, y, width, height);
   }
}