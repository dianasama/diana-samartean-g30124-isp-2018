package g30124.samartean.diana.l6.e3;

import javax.swing.*;
import java.awt.*;

public class Rectangle implements Shape{

    private int length, width, x, y;
    private String id;
    boolean isFilled;
    private Color color;

    public Rectangle(Color color, int length, int width,int x,int y,String id,boolean isFilled) {
        this.length = length;
        this.width = width;
    }
    
    
    @Override
    public int getX() {
    	return x;
    }
    
    @Override
    public int getY() {
    	return y;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    @Override 
    public Color getColor() {
    	return color;
    }
    
    @Override 
    public boolean isFilled() {
    	return isFilled;
    }
    

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle with length "+ length +" "+"and width " + width + " " +
    getColor().toString());
        g.setColor(getColor());
        g.drawRect(getX(),getY(),length,width);
        if(isFilled()==true)
            g.fillRect(getX(),getY(),length,width);
    }

}
