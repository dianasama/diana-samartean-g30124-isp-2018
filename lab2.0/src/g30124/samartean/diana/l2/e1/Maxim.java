package g30124.samartean.diana.l2.e1;

import java.util.Scanner;

public class Maxim {
	public static void main(String [] args)
	{   
		int a,b;
		Scanner in = new Scanner(System.in);
		
		System.out.println("Dati a = ");
		a = in.nextInt();
		
		System.out.println("Dati b= ");
		b = in.nextInt();
		
		in.close();
		
		if(a>b)
			System.out.println("Maximul este a= "+a);
		else
			System.out.println("Maximul este b= "+b);
	}
}
