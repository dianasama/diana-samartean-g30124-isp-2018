package g30124.samartean.diana.l2.e6;

import java.util.Scanner;

public class Factorial {
	//nerecursive method
		static int fact(int n){
			int i,s=1;
			for(i=1; i<=n;i++)
				s*=i;
			return s;
		}
		
		//recursive method
		static int fact2(int n){
			if (n==1) return 1;
			else return n*fact2(n-1);
		}
		
		public static void main(String[] args){
			int n;
			
			Scanner in = new Scanner(System.in);
			System.out.println("Dati n= ");
			n = in.nextInt();
			in.close();
			
			System.out.println("Factorialul lui n calculat iterativ: "+fact(n));
			System.out.println("Factorialul lui n calculat recursiv: "+fact2(n));
		}
	}



