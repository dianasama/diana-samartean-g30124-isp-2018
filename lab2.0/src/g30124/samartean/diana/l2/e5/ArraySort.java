package g30124.samartean.diana.l2.e5;

import java.util.Random;

public class ArraySort {
	 static void bubbleSort(int[] arr) {  
	        int n = arr.length;  
	        int aux = 0;  
	         for(int i=0; i < n; i++){  
	                 for(int j=1; j < (n-i); j++){  
	                          if(arr[j-1] > arr[j]){  
	                                
	                                 aux = arr[j-1];  
	                                 arr[j-1] = arr[j];  
	                                 arr[j] = aux;  
	                         }  
	                          
	                 }  
	         }  
}

	 public static void main(String[] args){
		 
		 int[] arr = new int[10];
		  Random r = new Random();
		  
		  System.out.println("Vectorul inainte de sortare: " );
		  for(int i=0; i<10; i++){
			  arr[i] = r.nextInt();
			  System.out.println(+arr[i] );
		  }
		  
		  System.out.println("Vectorul dupa sortare: " );
		  
		  bubbleSort(arr);
		  
		  for(int i=0; i<10; i++){
			  System.out.println(+arr[i] );
		  }
	 }

}
