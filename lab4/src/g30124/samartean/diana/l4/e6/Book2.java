package g30124.samartean.diana.l4.e6;

import java.util.ArrayList;

import g30124.samartean.diana.l4.e4.Author;

public class Book2 {
		 private String name;
		 private ArrayList<Author> authors;
		 private double price;
		 private int qtyInStock;

		    public Book2(String name, ArrayList<Author> authors, double price){
		        this.authors = authors;
		        this.price = price;
		        this.name = name;
		        this.qtyInStock = 0;
		    }

		    public Book2(String name, ArrayList<Author> authors, double price, int qtyInStock){
		        this.authors = authors;
		        this.price = price;
		        this.name = name;
		        this.qtyInStock = qtyInStock;
		    }

		    public String getName() {
		        return name;
		    }

		    public ArrayList<Author> getAuthors() {
		        return this.authors;
		    }

		    public double getPrice() {
		        return price;
		    }

		    public void setPrice(double price) {
		        this.price = price;
		    }

		    public void setQtyInStock(int qtyInStock) {
		        this.qtyInStock = qtyInStock;
		    }

		    @Override
		    public String toString() {
		        return this.name + " by " + this.authors.size() + "authors.";
		    }

	}


