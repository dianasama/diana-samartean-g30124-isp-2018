package g30124.samartean.diana.l4.e6;

import java.util.ArrayList;

import g30124.samartean.diana.l4.e4.Author;

import g30124.samartean.diana.l4.e5.Book;

public class RunBook2 {
	public static void main(String[] args) {
		Author a1= new Author("Maria", "maria.s@yahoo.com", 'f');
		Author a2= new Author("Alexandru", "alex@yahoo.com", 'm');
		ArrayList<Author> authors = new ArrayList<Author>();
		authors.add(a1);
	    authors.add(a2);
	    Book2 b1 = new Book2("Book", authors, 35);
	    System.out.println(b1.toString());

}
}