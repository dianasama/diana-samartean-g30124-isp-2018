package g30124.samartean.diana.l4.e7;

import g30124.samartean.diana.l4.e3.Circle;

public class Cilinder extends Circle{
	private double heigh= 1.0;
	private double volume;
	
	public Cilinder(double radius, double heigh, String color){
        super(radius,color);
        this.heigh = heigh;
	}

	public void getHeight(double heigh) {
		this.heigh= heigh;
	}
	
	public void getVolume(double volume) {
		this.volume= volume;
	}
	
	@Override
	public double getArea() {
	return this.heigh * 2 * 3.14 * this.getRadius();
}
}