package g30124.samartean.diana.l4.e8;

public class Shape {
	private String colour;
	private boolean filled;
	
	public Shape() {
		this.colour= "green";
		this.filled= true;
	}
	
	public Shape (String colour, boolean filled) {
		this.colour= colour;
		this.filled= filled;
	}
	
	public String getColour() {
        return colour;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    
    @Override
    public String toString() {
        if (this.filled == true)
            return "A Shape with colour of " + this.colour + " and filled.";
        else
            return "A Shape with colour of " + this.colour + " and NOT filled.";
    }

}
