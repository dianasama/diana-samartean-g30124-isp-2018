package g30124.samartean.diana.l4.e8;

public class Circle2 extends Shape {
	   private double radius;

	    public Circle2(){
	        super();
	        this.radius = 1.0;
	    }
	    
	    public Circle2(double radius){
	        super();
	        this.radius = radius;
	    }

	    public Circle2(double radius, String color, boolean filled){
	        super(color,filled);
	        this.radius = radius;
	    }

	    public double getArea(){
	        return (3.14 * (this.radius * this.radius));
	    }

	    public double getPerimeter(){
	        return 4 * 3.14 * this.radius;
	    }

	    public double getRadius() {
	        return radius;
	    }

	    public void setRadius(double radius) {
	        this.radius = radius;
	    }

	    @Override
	    public String toString(){
	        return "A Circle with radius = " + this.radius + " wich is a subclass of " + super.toString();
	    }
	}


