package g30124.samartean.diana.l4.e8;

public class Rectangle extends Shape{
	 private double width;
	 private double length;
	 
	 public Rectangle(){
	        super();
	        this.width = 1.0;
	        this.length = 1.0;
	 }
	 
	 public Rectangle(double length, double width){
	        super();
	        this.length = length;
	        this.width = width;
	    }
	 
	 public Rectangle(String color, boolean filled, double length, double width){
	        super(color,filled);
	        this.width = width;
	        this.length = length;
	    }
	 
	    public double getLength() {
	        return length;
	    }

	    public double getWidth() {
	        return width;
	    }

	    public double getArea(){
	        return this.length * this.width;
	    }

	    public double getPerimeter(){
	        return 2*this.width + 2*this.length;
	    }
	    
	    @Override
	    public String toString() {
	        return "A Rectangle with width = " + this.width + " and length = " + this.length + " ,wich is a subclass of " + super.toString();
	    }

	    public void setLength(double length) {
	        this.length = length;
	    }

	    public void setWidth(double width) {
	        this.width = width;
	    }
}
	 
