package g30124.samartean.diana.l4.e8;

public class RunShape{ 
	public static void main(String[] args) {
		 Circle2 c1 = new Circle2(1);
	        System.out.println(c1.toString());
	        System.out.println("Circle's area =  " + c1.getArea());

	        Rectangle r1 = new Rectangle(2,5);
	        System.out.println(r1.toString());
	        r1.setWidth(4);
	        r1.setFilled(false);
	        System.out.println(r1.toString());

	        Square sq1 = new Square(2);
	        sq1.setLength(20);
	        sq1.setColour("Rosu");
	        System.out.println(sq1.toString());

	        sq1.setWidth(5);
	        System.out.println("Square's area = " + sq1.getArea());
	        System.out.println("Square's perimeter = " + sq1.getPerimeter());
	}

}
