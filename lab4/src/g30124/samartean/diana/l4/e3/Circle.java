package g30124.samartean.diana.l4.e3;

public class Circle {
	private double radius;
	private String colour;
	
	public Circle() {
		this.radius= 1.0;
		this.colour= "red";
	}
	
	private Circle(double radius) {
		this.radius= radius;
	} 
	
	public Circle(double radius2, String color) {
		// TODO Auto-generated constructor stub
	}

	protected double getRadius() {
		return radius;
	}
	
	public double getArea() {
		return 2*3.14*radius;
	}

public static void main(String [] args) {
	Circle c= new Circle();
	System.out.println(c.getRadius());
	System.out.println (c.getArea());
}
}