package g30124.samartean.diana.l3.e4;

import becker.robots.*;

public class Figure {

	public static void main (String[] args) {
		  City viena = new City();
	      Wall v0 = new Wall(viena, 1, 1, Direction.WEST);
	      Wall v1 = new Wall(viena, 2, 1, Direction.WEST);
	      Wall v2 = new Wall(viena, 1, 2, Direction.EAST);
	      Wall v3 = new Wall(viena, 2, 2, Direction.EAST);
	      Wall v4 = new Wall(viena, 1, 1, Direction.NORTH);
	      Wall v5 = new Wall(viena, 1, 2, Direction.NORTH);
	      Wall v6 = new Wall(viena, 2, 1, Direction.SOUTH);
	      Wall v7 = new Wall(viena, 2, 2, Direction.SOUTH);
	      Robot Rob2 = new Robot(viena, 0, 2, Direction.WEST);
	 
			// Direct the robot to the final situation
	      Rob2.move();
	      Rob2.move();
	      Rob2.turnLeft();	// start turning right as three turn lefts
	      Rob2.move();
	      Rob2.move();
	      Rob2.move();
	      Rob2.turnLeft();
	      Rob2.move();
	      Rob2.move();
	      Rob2.move();
	      Rob2.turnLeft();
	      Rob2.move();
	      Rob2.move();
	      Rob2.move();
	      Rob2.turnLeft();
	      Rob2.move();

	}

}
