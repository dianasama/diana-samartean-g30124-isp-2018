package g30124.samartean.diana.l3.e3;

import becker.robots.*;

public class TurnAround {

	public static void main (String[] args) {
		 // Set up the initial situation
		  City cluj = new City();
	      //Thing parcel = new Thing(cluj, 1, 2);
	      Robot Rob1 = new Robot(cluj, 1, 1, Direction.NORTH);
	 
			// Direct the robot to the final situation

	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
	      Rob1.turnLeft();
	      Rob1.turnLeft();
	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
	      Rob1.move();
}
}
