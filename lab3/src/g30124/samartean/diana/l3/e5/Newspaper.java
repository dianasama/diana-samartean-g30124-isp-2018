package g30124.samartean.diana.l3.e5;

import becker.robots.*;

public class Newspaper {
	
	public static void main (String[] args) {
		City sibiu = new City();
		Thing parcel = new Thing(sibiu, 2, 2);
		Wall o0 = new Wall(sibiu, 1, 1, Direction.WEST);
		Wall o1 = new Wall(sibiu, 2, 1, Direction.WEST);
		Wall o2 = new Wall(sibiu, 1, 2, Direction.EAST);
		Wall o3 = new Wall(sibiu, 1, 1, Direction.NORTH);
		Wall o4 = new Wall(sibiu, 1, 2, Direction.NORTH);
		Wall o5 = new Wall(sibiu, 2, 1, Direction.SOUTH);
		Wall o6 = new Wall(sibiu, 1, 2, Direction.SOUTH);
		Robot karel = new Robot(sibiu, 1, 2, Direction.SOUTH);

		// Direct the robot to the final situation
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.move();
		karel.pickThing();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
		karel.move();
		karel.turnLeft();
		karel.turnLeft();
		karel.turnLeft();
	}	

}



