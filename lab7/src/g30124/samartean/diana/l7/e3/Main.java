package g30124.samartean.diana.l7.e3;

import java.util.Iterator;
import java.util.Set;

import g30124.samartean.diana.l7.e2.Bank;

public class Main {
	static void displayAll(Set list){
		Iterator i=list.iterator();

		while(i.hasNext()){
			String s=(String)i.next();
			System.out.println(s);
    }
}
    public static void main(String[] args) {
        Bank bank=new Bank();
        
        bank.addAccount("Mihai",562);
        bank.addAccount("Raluca",250);
        bank.addAccount("Cristi",350.38);
        bank.printAccounts();
    }

}
