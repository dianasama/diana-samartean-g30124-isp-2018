package g30124.samartean.diana.l7.e4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws IOException {
        Dictionary dictionary=new Dictionary();
        char choice;
        Word word;
        Definition definition;
        String line;
        BufferedReader fluxIn=new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("MENU");
            System.out.println("a - Add word.");
            System.out.println("s - Search word.");
            System.out.println("d - Display words.");
            System.out.println("l - List all definitions.");
            System.out.println("w - List all words.");
            System.out.println("e - Exit.");
            
            line=fluxIn.readLine();
            choice=line.charAt(0);
            
            switch (choice) {
               
            case 'a': 
                System.out.println("Add a word: ");
                line=fluxIn.readLine();
                if(line.length()>1) { 
                	word=new Word(line);
                    System.out.println("Add a definition: ");
                    line =fluxIn.readLine();
                    definition=new Definition(line);
                    dictionary.addWord(word,definition);
                } break;
               
            case 'l': 
                    dictionary.getAllDefinitions(); break;

            case 'w': 
                    dictionary.getAllWords();
                
            case 's':
                System.out.println("The word you've been searching for.");
                line=fluxIn.readLine();
                if(line.length()>1) {
                    word=new Word(line);
                    Definition def=dictionary.getDefinition(word);
                    if(def != null)
                        System.out.println("Definition: " + def);
                    else
                        System.out.println("There is no definition.");
                } break;
                
            case 'd':
                System.out.println("Display: ");
                dictionary.getAllWords();
                break;
           }
        } while ( choice != 'e');
    }

}
