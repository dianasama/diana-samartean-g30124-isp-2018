package g30124.samartean.diana.l7.e4;

import java.util.HashMap;
import java.util.Iterator;

public class Dictionary {
	HashMap dict=new HashMap();
    
    public void addWord(Word word,Definition definition) {
        if(dict.containsKey(word))
            System.out.println("The word already exists.");
        else
            System.out.println("Add a word.");
        dict.put(word,definition);
    }

    public Definition getDefinition(Word word) {
        System.out.println(dict.containsKey(word));
        return (Definition) dict.get(word);
    }

    public void getAllWords() {
    	dict.forEach((key,value)-> System.out.println(key));
    }
   
    public void getAllDefinitions() {
        dict.forEach((key, value) -> System.out.println( value));
    }

   @Override
    public String toString() {
        return "Dictionary {" + "dictionar = " + dict + "}";
    }

}
