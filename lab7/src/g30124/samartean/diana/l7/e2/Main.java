package g30124.samartean.diana.l7.e2;

import java.util.ArrayList;
import java.util.Collections;

import g30124.samartean.diana.l7.e1.*;

public class Main {
	    public static void main(String[] args) {
	       Bank bank=new Bank();

	       bank.addAccount("Mircea",400);
	       bank.addAccount("Ana",400.4);
	       bank.addAccount("Bogdan",2000);
	       
	       bank.printAccounts();
	        System.out.println("-----");

	        ArrayList<BankAccount> bankAccounts= bank.getAccounts();

	        Collections.sort(bankAccounts,BankAccount.BankAccountComparator);
	        for(BankAccount b:bankAccounts)
	            System.out.println(b.toString());

	    }
}


