package g30124.samartean.diana.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import g30124.samartean.diana.l7.e1.*;

public class Bank {
	ArrayList<BankAccount> accounts=new ArrayList<>();
    public void addAccount(String owner,double balance)
    {
        BankAccount b=new BankAccount(owner,balance);
        accounts.add(b);
        }
        public void printAccounts()
        {   sortList();
            for(BankAccount b:accounts)
                System.out.println(b.toString());
        }
        public void sortList()
        {
            Collections.sort(accounts);
        }
        public void printAccounts(double minBalance,double maxBalance)
        {   sortList();
                for(BankAccount b:accounts)
                {
                    if(b.getBalance() > minBalance && b.getBalance() < maxBalance)
                        System.out.println(b.toString());
                }
        }
        public BankAccount getAccount(String owner)
        {
            Iterator<BankAccount> iterator=accounts.iterator();
            while(iterator.hasNext())
            {
                BankAccount b= iterator.next();
                if(b.getOwner().equals(owner))
                    return b;

            }
            return null;
        }

    public ArrayList<BankAccount> getAccounts() {
        return accounts;
    }

}
