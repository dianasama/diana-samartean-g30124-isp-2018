package g30124.samartean.diana.l7.e1;

import java.util.Comparator;
import java.util.Objects;


public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;

    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
    	return balance;
    }

    public void withdraw(double amount)
    {
        if(balance>0 && balance>amount)
            balance-=amount;
    }
    public void deposit(double amount)
    {
        if(amount>0)
            balance+=amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return that.balance==balance && that.owner.equals(owner);
               // Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
    	return Objects.hash(owner, balance);
    }

    @Override
    public int compareTo(BankAccount o) {
        return  (int)(balance-o.balance);
    }
    
    @Override
    public String toString() {
        return "BankAccount " + "owner: " + owner + ", balance = " + balance;
    }

    public static Comparator<BankAccount> BankAccountComparator=new Comparator<BankAccount>(){
        public int compare(BankAccount b1,BankAccount b2)
        {
            String bankName1=b1.getOwner().toUpperCase();
            String bankName2=b2.getOwner().toUpperCase();

            return bankName1.compareTo(bankName2);
        }
 };
}

