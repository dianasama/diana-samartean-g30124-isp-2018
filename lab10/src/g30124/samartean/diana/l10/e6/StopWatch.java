package g30124.samartean.diana.l10.e6;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.*;

public class StopWatch extends JFrame{
    long currentTime;    
    long displayTime;   
    Timer timer;        
    State state;         
                         
    JButton startStop;   
    JButton lap;         
    JTextField display; 

  
    public StopWatch() {
        super ("StopWatch");
        
      
        startStop = new JButton("Start/Stop");
        startStop.addActionListener(new startStopController());
      
      
        lap = new JButton("Lap");
        lap.addActionListener(new lapController());
      
      
        display = new JTextField("00:00:0");
	display.setEditable(false);


        Container c = getContentPane();
        c.setLayout (new FlowLayout () );
        c.add(startStop);
        c.add(lap);
        c.add(display);

        
       	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);     
        setSize( 120, 150);

       
        currentTime = 0;
        displayTime = 0;
        timer = new Timer(100, new TimerHandler ());
        state = new Stopped();
        timer.setRepeats(true);
        timer.start();
       
       
        show();
    }
 
    
    public long getCurrentTime () {
    	return currentTime;}
    
    public void setCurrentTime (long timeArg) {
    	currentTime = timeArg;}
    
    public long getDisplayTime () {
    	return displayTime;}
   
  
    public void setDisplayTime (long timeArg) {
        displayTime = timeArg;
        
        
        String minutes = Long.toString((displayTime/600)%60);
        if ((displayTime/600)%60 < 10) minutes = "0"+ minutes;
        
        
        String seconds = Long.toString((displayTime/10%60));
        if ((displayTime/10%60) < 10) seconds = "0" + seconds;
        
        
        String tenths = Long.toString(displayTime%10);
        
        
        display.setText( minutes + ":" + seconds + ":" + tenths);
    }
  
    
    public interface State {
        public void updateTime();      
        public void startStopPushed(); 
        public void lapPushed();      
    }
    
   
    public class Stopped implements State {
        public void updateTime () { }
        public void startStopPushed() {
            state = new Started();
        }
        public void lapPushed() {
            setCurrentTime(0);
            setDisplayTime(0);
        }        
    }

    
    public class LapAndStopped implements State {
        public void updateTime () { }
        public void startStopPushed() {
            state = new LapAndStarted();
        }
        public void lapPushed() {
            setDisplayTime(getCurrentTime());
            state = new Stopped();
        }        
    }


    public class Started implements State {
        public void updateTime () {
            setCurrentTime(getCurrentTime()+1);
            setDisplayTime(getCurrentTime());
        }
        public void startStopPushed() {
            state = new Stopped();
        }
        public void lapPushed() {
            state = new LapAndStarted();
        }        
    }

  
    public class LapAndStarted implements State {
        public void updateTime () { 
           setCurrentTime(getCurrentTime()+1);
        }
        public void startStopPushed() {
            state = new LapAndStopped();
        }
        public void lapPushed() {
           setDisplayTime(getCurrentTime());
           state = new Started();
        }        
    }

    
    public class startStopController extends AbstractAction {
        public void actionPerformed (ActionEvent e) {
            state.startStopPushed();
        }
    }
    

    public class lapController extends AbstractAction {
        public void actionPerformed (ActionEvent e) {
            state.lapPushed();
        }
    }

   
    class TimerHandler implements ActionListener {
        public void actionPerformed (ActionEvent e) {
            state.updateTime();
        }        
    }
    
    
    public static void main (String args []) {
        new StopWatch();
    }        


}
