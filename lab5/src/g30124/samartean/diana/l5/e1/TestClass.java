package g30124.samartean.diana.l5.e1;

import static org.junit.Assert.*;
import org.junit.*;


public class TestClass {
	
	Shape[] s = new Shape[4];
	
	@Test
	public void TestShape() {
		s[0] = new Circle("blue", true, 4);
		System.out.println("Area and perimeter for Circle:");
		System.out.println(s[0].getArea());
		assertTrue(s[0].getArea() == 50.24);
		System.out.println(s[0].getPerimeter());
		assertTrue(s[0].getPerimeter() == 25.12);
		
		s[1] = new Rectangle(3,4,"green",false);
		System.out.println("Area and perimeter for Rectangle:");
		System.out.println(s[1].getArea());
		assertTrue(s[1].getArea() == 12);
		System.out.println(s[1].getPerimeter());
		assertTrue(s[1].getPerimeter() == 14);
		
		s[2] = new Square(5,"blue",false);
		System.out.println("Area and perimeter for Square:");
		System.out.println(s[2].getArea());
		assertTrue(s[2].getArea() == 25);
		System.out.println(s[2].getPerimeter());
		assertTrue(s[2].getPerimeter() == 20);
		
		
	}
		
	
	
}
