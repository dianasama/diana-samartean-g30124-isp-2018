package g30124.samartean.diana.l5.e1;

public class Square extends Rectangle {

	protected double side; 
	
	public Square() {
		super();
	}
	
	public Square(double side) {
		super(side,side);
		this.side = side;
		
	}
	
	public Square(double side, String color, boolean filled) {
		super(side, side,color,filled);
		if (side <= length && side <= width)
			this.side = side;
	}
	
	public double getSide(double side) {
		if (side <= length && side <= width)
		  if (length == width)
			  side = length;
		return side;
	}
	
	public void setSide(double side) {
		this.side = side;
	}
	
	@Override
	public void setWidth(double side) {
		super.width = side;
	}
	
	
	public void setLength(double length) {
		super.length = side;
	}
	
	@Override
	public String toString() {
		return "Square{ side: " + getSide(side) +  ", which is a subclass of " + super.toString() 
		+ "}";
	}
}

