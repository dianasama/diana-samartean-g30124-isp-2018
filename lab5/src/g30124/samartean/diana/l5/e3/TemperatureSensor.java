package g30124.samartean.diana.l5.e3;

import java.util.Random;
import static java.lang.Math.abs;

public class TemperatureSensor extends Sensor {

    private Random rand = new Random();

    public TemperatureSensor(String location){
        super(location);
    }

    @Override
    public int read(){
        return abs(rand.nextInt() % 100);
    }
}