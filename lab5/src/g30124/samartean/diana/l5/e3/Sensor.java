package g30124.samartean.diana.l5.e3;

public class Sensor {
	protected String location;

    public Sensor(String location) {
        this.location = location;
    }

    public int read(){
        return 0;
    }

    public String getLocation() {
        return location;
    }

}
