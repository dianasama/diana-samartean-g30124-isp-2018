package g30124.samartean.diana.l5.e3;

import java.util.Random;

public class LightSensor extends Sensor {

    private Random rand = new Random();

    public LightSensor(String location){
        super(location);
    }

    @Override
    public int read(){
        return abs(rand.nextInt() % 100);
    }

    private int abs(int i) {
		// TODO Auto-generated method stub
		return 0;
	}
}
