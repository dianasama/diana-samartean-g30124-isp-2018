package g30124.samartean.diana.l5.e2;

public class RotatedImage implements Image {
	   
		private RealImage realImage;
	    private String fileName;

	    public RotatedImage(String fileName){
	        this.fileName = fileName;
	    }

	    @Override
	    public void display() {
	        System.out.println("Display rotated" + this.fileName);
	    }
	}
