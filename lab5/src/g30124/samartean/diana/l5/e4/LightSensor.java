package g30124.samartean.diana.l5.e4;

import java.util.Random;

import static java.lang.Math.abs;

public class LightSensor extends Sensor {

    private Random rand = new Random();

    public LightSensor(String location){
        super(location);
    }

    @Override
    public int read(){
        return abs(rand.nextInt() % 100);
    }
}
