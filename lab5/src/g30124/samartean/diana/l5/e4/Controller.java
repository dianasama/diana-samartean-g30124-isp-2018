package g30124.samartean.diana.l5.e4;

import java.util.concurrent.TimeUnit;

public class Controller {
    
	private Sensor light_s = new LightSensor("light");
    private Sensor temp_s = new TemperatureSensor("temperature");

    public void display() {
    	 int i;
    	 
        for(i = 0; i < 20; i++){
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println("Light sensor: "+light_s.read());
                System.out.println("Temperature sensor: "+temp_s.read());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
       Controller c = new Controller();
        c.display();
    }
}
