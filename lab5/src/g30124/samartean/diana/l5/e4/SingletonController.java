package g30124.samartean.diana.l5.e4;

public class SingletonController extends Controller{
	   
	private static Controller oneCtrl = null;

    private SingletonController() {
        super();
        System.out.println("Controller created!");
    }

    public static Controller createController() {
        if (oneCtrl == null) {
            oneCtrl = new SingletonController();
        }
        else {
            System.out.println("Warning! Only one controller is alowed!");
        }
        return oneCtrl;
    }
}
