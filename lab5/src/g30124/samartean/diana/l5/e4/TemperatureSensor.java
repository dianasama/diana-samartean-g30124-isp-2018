package g30124.samartean.diana.l5.e4;

import java.util.Random;
import static java.lang.Math.abs;

public class TemperatureSensor extends Sensor {

    private Random rdn = new Random();

    public TemperatureSensor(String location){
        super(location);
    }

    @Override
    public int read(){
        return abs(rdn.nextInt() % 100);
    }
}

