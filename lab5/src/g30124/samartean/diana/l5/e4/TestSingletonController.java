package g30124.samartean.diana.l5.e4;

import org.junit.Test;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

public class TestSingletonController {

    @Test
    public void testSingleToneController(){
        Controller c01 = SingletonController.createController();
        Controller c02 = SingletonController.createController();
        assertNotNull(c01);
        assertEquals(c01,c02); 
    }
}
