package g30124.samartean.diana.l9.e2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Application implements ActionListener {

    JButton Btn = new JButton("Tap to increment");
    JFrame frm = new JFrame();  
    JTextField displayTF = new JTextField(15);

    int i = 0;

    public Application(){

        displayTF.setText(""+i);
        frm.setTitle("Incrementation");
        frm.setVisible(true);
        frm.setSize(200,100);
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setResizable(true);
        frm.setLayout(new FlowLayout());
        frm.add(displayTF);
        frm.add(Btn);
        Btn.addActionListener(this);
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == Btn){
            i++;
            displayTF.setText(""+i);
        }
        
    }
    public static void main(String args[]){
        new Application();
    }


}